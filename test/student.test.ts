import request from "supertest"
import { Any, createConnections, getConnection } from "typeorm"
import app from "../src/app"
import Student from "../src/models/Student"

beforeAll(async () => {
    await createConnections()
})

afterAll(async () => {
    const defaultConnection = getConnection('default')
    await defaultConnection.close()
})

test('Deve validar se o post retorna o estudante adicionado', async () => {
    await request(app).delete('/student/usertest')
    const student = {
        user: "usertest",
        firstName: "firsttest001",
        lastName: "lasttest001",
        schoolName: "schooltest001",
        teacher: "teachertest001",
        class: "class",
        password: "passwordtest001"
    }
    const response = await request(app)
    .post('/student')
    .send(student)

    expect(response.body).toMatchObject(student)

    await request(app).delete('/student/usertest')
});

test('Deve validar se um usuário já foi pego', async () => {
    await request(app).delete('/student/usertest')
    const student = {
        user: "usertest",
        firstName: "firsttest001",
        lastName: "lasttest001",
        schoolName: "schooltest001",
        teacher: "teachertest001",
        class: "class",
        password: "passwordtest001"
    }
    await request(app).post('/student').send(student)

    const response = await request(app)
    .post('/student')
    .send(student)

    expect(response.body).toBe("Usuário já cadastrado")

    await request(app).delete('/student/usertest')
})

test('Deve validar se os gets não recebem as senhas', async () => {
    const response = await request(app)
    .get('/student')

    expect(response.body[0]).toEqual(expect.not.objectContaining({password: expect.anything()}))
})

test('Deve validar se o usuário é deletado com sucesso', async () => {
    const student = {
        user: "usertest",
        firstName: "firsttest001",
        lastName: "lasttest001",
        schoolName: "schooltest001",
        teacher: "teachertest001",
        class: "class",
        password: "passwordtest001"
    }
    await request(app).post('/student').send(student)

    const response = await request(app)
    .delete('/student/usertest')

    expect(204)
})

test('Deve validar se caso o usuário não existe ao ser deletado retorna erro', async () => {
    await request(app).delete('/student/usertest')
    const response = await request(app)
    .delete('/student/usertest')

    expect(response.body).toBe('Usuário não encontrado')
    expect(400)
})

test('Deve validar se consegue procurar pelo usuário', async () => {
    await request(app).delete('/student/usertest')
    const student = {
        user: "usertest",
        firstName: "firsttest001",
        lastName: "lasttest001",
        schoolName: "schooltest001",
        teacher: "teachertest001",
        class: "class",
        password: "passwordtest001"
    }
    await request(app).post('/student').send(student)

    const response = await request(app)
    .get('/student/usertest')

    expect(response.body).toEqual(expect.objectContaining({user: "usertest"}))
    await request(app).delete('/student/usertest')
})

test('Deve validar se caso procure por usuário não existente retorne erro', async () => {
    const response = await request(app).get('/student/dfjgndfigfdnmgiodfsngdfg')

    expect(response.body).toBe("Usuário dfjgndfigfdnmgiodfsngdfg não encontrado")
})

test('Deve validar se consegue procurar pela escola', async () => {
    await request(app).delete('/student/usertest')
    const student = {
        user: "usertest",
        firstName: "firsttest001",
        lastName: "lasttest001",
        schoolName: "school001",
        teacher: "teachertest001",
        class: "class",
        password: "passwordtest001"
    }
    await request(app).post('/student').send(student)

    const response = await request(app)
    .get('/student/school/school001')

    expect(response.body).toHaveLength(1)

    await request(app).delete('/student/usertest')
})

test('Deve validar se caso procure por usuário com escola não existente retorne erro', async () => {
    const response = await request(app).get('/student/school/dfjgndfigfdnmgiodfsngdfg')

    expect(response.body).toBe("Nenhum aluno da escola dfjgndfigfdnmgiodfsngdfg foi encontrado")
})