import request from "supertest"
import {createConnections, getConnection } from "typeorm"
import app from "../src/app"
import Question from "../src/models/Question"
import Student from "../src/models/Student"


beforeAll(async () => {
    await createConnections()
})

afterAll(async () => {
    const defaultConnection = getConnection('default')
    await defaultConnection.close()
})

test('Deve validar se o post retorna a questão adicionada', async () => {
    await request(app).delete('/question/Hist995678')
    const question = {
        id: "Hist995678",
        title: "Primeira Guerra 1",
        description: "Quem foi Adolf Hitler?",
        alternative1: "Presidente dos Estados Unidos",
        alternative2: "General japonês",
        alternative3: "Inventor da bomba atômica",
	    alternative4: "Líder do partido nazista",
	    answer: 4
    }
    const response = await request(app)
    .post('/question')
    .send(question)

    expect(response.body).toMatchObject(question)

    await request(app).delete('/question/Hist995678')
});

test("Deve Validar se consegue deletar uma questão", async () => {
    await request(app).delete('/question/Hist995678')
    const question = {
        id: "Hist995678",
        title: "Primeira Guerra 1",
        description: "Quem foi Adolf Hitler?",
        alternative1: "Presidente dos Estados Unidos",
        alternative2: "General japonês",
        alternative3: "Inventor da bomba atômica",
	    alternative4: "Líder do partido nazista",
	    answer: 4
    }
    const response = await request(app).post('/question').send(question)

    await request(app).delete('/question/Hist995678')
    expect(204)
})

test("Deve validar se caso a questão deletada não exista, retorne erro", async () => {
    const response = await request(app).delete('/question/4sd65fsfs4d65')
    expect(response.body).toBe("Questão não encontrada")
    expect(400)
})

test("Testa se o get de questões retorna questões", async () => {
    await request(app).delete('/question/Hist995678')
    const question = {
        id: "Hist995678",
        title: "Primeira Guerra 1",
        description: "Quem foi Adolf Hitler?",
        alternative1: "Presidente dos Estados Unidos",
        alternative2: "General japonês",
        alternative3: "Inventor da bomba atômica",
	    alternative4: "Líder do partido nazista",
	    answer: 4
    }
    await request(app).post('/question').send(question)

    const response = await request(app).get('/question')
    expect(response.body[0]).toMatchObject(new Question)

    await request(app).delete('/question/Hist995678')
})

test('Deve validar se ao completar a questão certa, o usuário recebe a questão', async () =>{
    await request(app).delete('/question/Hist995678')
    await request(app).delete('/student/usertest')
    const question = {
        id: "Hist995678",
        title: "Primeira Guerra 1",
        description: "Quem foi Adolf Hitler?",
        alternative1: "Presidente dos Estados Unidos",
        alternative2: "General japonês",
        alternative3: "Inventor da bomba atômica",
	    alternative4: "Líder do partido nazista",
	    answer: 4
    }
    const student = {
        user: "usertest",
        firstName: "firsttest001",
        lastName: "lasttest001",
        schoolName: "school001",
        teacher: "teachertest001",
        class: "class",
        password: "passwordtest001"
    }
    await request(app).post('/question').send(question)
    await request(app).post('/student').send(student)
    
    const response = await request(app).post('/question/Hist995678').send({
        "answer": 4,
        "student": {"user": "usertest"}
    })

    expect(response.body).toBe('Resposta certa, parabéns')

    const response2 = await request(app).get('/student/usertest')
    
    expect(response2.body.questions.length).toBe(1)

    await request(app).delete('/question/Hist995678')
    await request(app).delete('/student/usertest')
})

test('Deve validar se ao completar a questão errada, o usuário recebe a resposta errada', async () =>{
    await request(app).delete('/question/Hist995678')
    await request(app).delete('/student/usertest')
    const question = {
        id: "Hist995678",
        title: "Primeira Guerra 1",
        description: "Quem foi Adolf Hitler?",
        alternative1: "Presidente dos Estados Unidos",
        alternative2: "General japonês",
        alternative3: "Inventor da bomba atômica",
	    alternative4: "Líder do partido nazista",
	    answer: 4
    }
    const student = {
        user: "usertest",
        firstName: "firsttest001",
        lastName: "lasttest001",
        schoolName: "school001",
        teacher: "teachertest001",
        class: "class",
        password: "passwordtest001"
    }
    await request(app).post('/question').send(question)
    await request(app).post('/student').send(student)
    
    const response = await request(app).post('/question/Hist995678').send({
        "answer": 0,
        "student": {"user": "usertest"}
    })

    expect(response.body).toBe('Resposta errada :(')
    await request(app).delete('/question/Hist995678')
    await request(app).delete('/student/usertest')
})

test('Deve validar se ao completar a questão errada, o usuário recebe a resposta errada', async () =>{
    await request(app).delete('/question/Hist995678')
    const response = await request(app).post('/question/Hist995678').send({
        "answer": 0,
        "student": {"user": "usertest"}
    })

    expect(response.body).toBe('Questão não encontrada')
})