import { Router } from "express"
import { getCustomRepository, getRepository } from "typeorm"
import Student from "../models/Student";
import StudentRepository from "../repositories/StudentRepository";
import cors from "cors"

const cors = require("cors")

const studentRouter = Router()

studentRouter.use(cors())

studentRouter.post('/', async (req, res) => {
    //try {
        const repo = getCustomRepository(StudentRepository);
        const user = await repo.findByUser(req.body.user)
        if (!user){
            const response = await repo.save(req.body)
            return res.status(201).json(response)
        }
        const response = `Usuário já cadastrado`
        return res.status(400).json(response)
/*
    } catch (err){
        console.log("Erro: ", (err.message))
    }
*/
})

studentRouter.get('/', async (req, res) => {
    const repo = getCustomRepository(StudentRepository)
    const estudantes: Student[] = await repo.find()
    const response = await repo.deleteToFront(estudantes)
    return res.json(response)
});

studentRouter.get('/:user', async (req,res) => {
    const repo = getCustomRepository(StudentRepository);
    const user = await repo.findByUser(req.params.user)
    const response = await repo.deleteOneToFront(user)
    if (response){
        return res.json(response)
    } else {
        return res.json(`Usuário ${req.params.user} não encontrado`)
    }
})

studentRouter.get('/school/:school', async (req,res) => {
    const repo = getCustomRepository(StudentRepository);
    const user = await repo.findBySchool(req.params.school)
    const response = await repo.deleteToFront(user)
    if (response.length > 0){
        return res.json(response)
    } else {
        return res.json(`Nenhum aluno da escola ${req.params.school} foi encontrado`)
    }
})

studentRouter.delete('/:user', async (req, res) => {
    const repo = getCustomRepository(StudentRepository)
    const userFound = await repo.findByUser(req.params.user)
    if (!userFound){
        return res.status(400).json("Usuário não encontrado")
    }
    const response = await repo.delete({user: req.params.user})
    return res.status(204).json(response)
})

export default studentRouter;