import { Router } from "express";
import { close } from "inspector";
import { getCustomRepository, getRepository } from "typeorm";
import Question from "../models/Question"
import Student from "../models/Student";
import { QuestionRepository } from "../repositories/QuestionRepository";
import StudentRepository from "../repositories/StudentRepository";
import cors from "cors"

const cors = require("cors")

const questionRouter = Router()

questionRouter.use(cors())

questionRouter.get('/', async (req, res) => {
    res.json(await getRepository(Question).find());
});

questionRouter.post('/', async (req, res) =>{
    const repo = getRepository(Question)
    const response = await repo.save(req.body)
    return res.status(201).json(response)
})

questionRouter.delete('/:id', async (req, res) =>{
    const repo = getCustomRepository(QuestionRepository)
    const questionFound = await repo.findById(req.params.id)
    if (!questionFound){
        return res.status(400).json("Questão não encontrada")
    }
    const response = await repo.delete({id: req.params.id})
    return res.status(204).json(response)
})

questionRouter.post('/:id', async (req, res) => {
    const repo = getCustomRepository(QuestionRepository)
    const questionFound = await repo.findById(req.params.id)
    if (!questionFound){
        return res.status(400).json("Questão não encontrada")
    }
    if (repo.correctAnswer(questionFound, req.body.answer, req.body.student)){
        const studentRepo = getCustomRepository(StudentRepository)
        const questionRepo = getCustomRepository(QuestionRepository)
        const questionId = questionFound.id
        
        const estudante = await studentRepo.findByUser(req.body.student.user)

        estudante.questions.push(questionFound)

        await studentRepo.save(estudante)
        res.json('Resposta certa, parabéns')
    } else {
        res.json('Resposta errada :(')
    }
})
export default questionRouter;