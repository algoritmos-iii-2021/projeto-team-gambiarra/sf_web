import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class UpdateQuestion1623325326522 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            "question", new TableColumn({
                name: "answer",
                type: "int"
            })
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn("question", "answer")
    }

}
