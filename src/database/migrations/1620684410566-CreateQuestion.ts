
import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateQuestions1620684410566 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: "question",
                columns: [
                    {
                        name: "id",
                        type: "varchar",
                        isPrimary: true
                    },
                    {
                        name: "title",
                        type: "varchar"
                    },
                    {
                        name: "description",
                        type: "varchar"
                    },
                    {
                        name: "alternative1",
                        type: "varchar"
                    },
                    {
                        name: "alternative2",
                        type: "varchar"
                    },
                    {
                        name: "alternative3",
                        type: "varchar"
                    },
                    {
                        name: "alternative4",
                        type: "varchar"
                    }
                ]
            })
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        queryRunner.dropTable("question")
    }

}
