import { response } from "express";
import { EntityRepository, Repository } from "typeorm";
import Student from "../models/Student"

@EntityRepository(Student)
export default class StudentRepository extends Repository<Student> {
    public async findByUser(user: string): Promise<Student>{
        const userFound = this.findOne({
            where:{user}
        })
        return userFound
    }
    public async findBySchool(schoolName: string): Promise<Student[]>{
        const userFound = this.find({
            where:{schoolName}
        })
        return userFound
    }
    public async deleteToFront(estudantes: Student[]): Promise<Student[]>{
        const tamanho = estudantes.length
        for (let i=0;i < tamanho;i++){
            delete estudantes[i].password
            delete estudantes[i].createdAt
            delete estudantes[i].updatedAt
        }
        return estudantes
    }
    public async deleteOneToFront(estudante: Student): Promise<Student>{
        if (estudante){
            delete estudante.password
            delete estudante.createdAt
            delete estudante.updatedAt
        }
            return estudante
    }

}