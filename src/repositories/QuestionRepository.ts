import {EntityRepository, Repository} from "typeorm";
import Question from "../models/Question";
import Student from "../models/Student";


@EntityRepository(Question)
export class QuestionRepository extends Repository<Question> {

    findById(id: string) {
        const questionFound = this.findOne({
            where:{id}
        })
        return questionFound
    }
    correctAnswer(questionFound: Question, answer: number, student: Student) {
        if (questionFound.answer == answer){
            return true
        } else {
            return false
        }
    }

}