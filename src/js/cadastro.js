const frm = document.querySelector("form");
const divAlert = document.querySelector(".alert");

frm.addEventListener("submit", async (e) => {
  e.preventDefault();

  const url = "http://localhost:3000/student";

  const body = {
    user: frm.user.value,
    firstName: frm.firstName.value,
    lastName: frm.lastName.value,
    schoolName: frm.schoolName.value,
    teacher: frm.teacher.value,
    class: frm.class.value,
    password: frm.password.value
}

const response = await fetch(url, {
  method:"POST",
  body: JSON.stringify(body),
  headers: {"Content-Type":"application/json"}
})

if (response.status == 201) {
  alert(`Cadastro Realizado com sucesso! Bem vindo ${body.user}`)
  window.location.reload()
} else {
  alert(`Informações invalidas. Favor verifique os campos do formulário`)
}
});

