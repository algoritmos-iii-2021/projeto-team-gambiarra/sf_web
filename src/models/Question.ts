import {Column, Entity, ManyToMany, PrimaryColumn} from "typeorm";
@Entity()
export default class Question {

    @PrimaryColumn({
        length: 20,
        unique: true
    })
    id: string;

    @Column({
        length: 20
    })
    title: string

    @Column({
        length: 200
    })
    description: string

    @Column({
        length: 50
    })
    alternative1: string

    @Column({
        length: 50
    })
    alternative2: string

    @Column({
        length: 50
    })
    alternative3: string

    @Column({
        length: 50
    })
    alternative4: string

    @Column()
    answer: number

}
